class SWAPI::Film < SWAPI::Base
  attr_reader :title, :url

  def self.all
    @all ||= get_and_parse_response_body(root.films).results.map do |film|
      self.new(film)
    end
  end

  def initialize(film)
    @title = film.title
    @url = film.url
  end

  def people
    @people ||= SWAPI::Person.all.find_all { |person| person.films.include?(url) }
  end

  def to_partial_path
    "films/film"
  end
end