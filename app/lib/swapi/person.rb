class SWAPI::Person < SWAPI::Base
  attr_reader :name, :mass, :homeworld, :films

  # filter out people with a mass <= 75
  def self.all
    @all ||= pages.flat_map do |page|
      page.results.filter_map { |person| self.new(person) if person.mass.to_i > 75 }
    end
  end

  def initialize(person)
    @name = person.name
    @mass = person.mass
    @homeworld = SWAPI::Planet.new(person.homeworld).name
    @films = person.films
  end

  def to_partial_path
    "people/person"
  end

  private

  # Get all pages of the people resources
  def self.pages
    return @pages if @pages

    @pages = []
    @pages << get_and_parse_response_body(root.people)

    while(@pages.last.next.present?)
      @pages << get_and_parse_response_body(@pages.last.next)
    end

    @pages
  end
end