class SWAPI::Planet < SWAPI::Base
  attr_reader :name

  def initialize(planet_url)
    @name = self.class.get_and_parse_response_body(planet_url).name
  end
end