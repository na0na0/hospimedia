class SWAPI::Base
  private

  attr_reader :connection

  URL = 'https://swapi.dev/api/'

  def self.connection
    @connection ||= Faraday.new(url: URL, headers: { 'Content-Type' => 'application/json' })
  end

  # Get each url on all available resources within the API.
  def self.root
    @root ||= get_and_parse_response_body
  end

  def self.get_and_parse_response_body(url = nil)
    response = connection.get(url)
    JSON.parse(response.body, object_class: OpenStruct) if response.status == 200
  end
end